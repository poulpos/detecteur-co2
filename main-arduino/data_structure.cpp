#include "data_structure.h"

#include <stdlib.h>
#include <TFT_eSPI.h>

template<typename X, typename Y>
int CircularBufferXY<X,Y>::inc(int i) const {
  return (i < size-1)?i+1:0 ;
}

template<typename X, typename Y>
CircularBufferXY<X,Y>::CircularBufferXY(int size_) {
  size = size_ ;
  tab_x = (X*)calloc(size, sizeof(X)) ;
  tab_y = (Y*)calloc(size, sizeof(Y)) ;
  for(int i = 0 ; i < size ; i++) {
    tab_x[i] = i ;
    tab_y[i] = i ;
  }
  y_0 = 0 ;
  gradient = 0 ;
  clean() ;
}

// add (x,y) to the buffers
// overwrites the oldest values if needed
template<typename X, typename Y>
void CircularBufferXY<X,Y>::add_hard(X x, Y y) {
  tab_x[i_insert] = x ;
  tab_y[i_insert] = y ;
  
  i_insert = inc(i_insert) ;
  if (!full) {
    if (i_insert == i_start) {
      full = true ;
    }
  } else {
    i_start = inc(i_start) ;
  }

  Serial.printf("start: %d\n", i_start);
  Serial.printf("insert: %d\n", i_insert);
  
}

// add (x, y) to the buffers  if there is empty space
template<typename X, typename Y>
void CircularBufferXY<X,Y>::add_soft(X x, Y y) {
  if (!full) {
    add_hard(x, y);
  }
}


template<typename X, typename Y>
void CircularBufferXY<X,Y>::clean() {
  i_start = 0 ;
  i_insert = 0 ;
  full = (size==0)?true:false ;
}

template<typename X, typename Y>
bool CircularBufferXY<X,Y>::is_full() const {
  return full ;
}

// compute this.y_0 and this.gradient such that
// y = y_0 + gradient * x is a linear regression of
// the points whose coordinates are in the buffers
template<typename X, typename Y>
void CircularBufferXY<X,Y>::make_lin_reg() {
  if (!full && i_start == i_insert) {
    y_0 = 0 ;
    gradient = 0 ;
  }

  double y_ave = 0 ;
  double x_ave = 0 ;
  int i, nb ;
  nb = 0 ;
  for(i = full?inc(i_start):i_start ; i != i_insert ; i = inc(i)) {
    x_ave += tab_x[i] ;
    y_ave += tab_y[i] ;
    nb++ ;
  }
  x_ave /= nb ;
  y_ave /= nb ;
  
  double c;
  gradient = 0 ;
  c = 0 ;
  for(i = full?inc(i_start):i_start ; i != i_insert ; i = inc(i)) {
    gradient += (tab_x[i] - x_ave)*(tab_y[i] - y_ave);
    c += (tab_x[i] - x_ave)*(tab_x[i] - x_ave);
  }
  gradient = gradient / c ;
  y_0 = y_ave - gradient * x_ave ;
  Serial.println("y_0: ");
  Serial.println(this->y_0);
  Serial.println("gradient: ");
  Serial.println(this->gradient);
}

// returns the image of x_ by the linear regression
template<typename X, typename Y>
double CircularBufferXY<X,Y>::lin_reg(double x_) const {
  return y_0 + gradient * x_ ;
}

// returns the inverse of y_ by the linear regression
template<typename X, typename Y>
double CircularBufferXY<X,Y>::inv_lin_reg(double y_) const {
  return (y_ - y_0) / gradient ;
}



template<typename X, typename Y>
void CircularBufferXY<X,Y>::toSerial() const {
  int i ;

  Serial.println("---------------------------");
  for(i = full?inc(i_start):i_start ; i != i_insert ; i = inc(i)) {
    Serial.println(i);
    Serial.println(roundf(tab_x[i]));
    Serial.println(roundf(tab_y[i]));
  }
}

template<typename X, typename Y>
CBXYSprite<X,Y>::CBXYSprite(uint16_t w, uint16_t h, X wxu, Y hyu, X mx, Y my, TFT_eSPI* tft, int buffer_size) : CircularBufferXY<X, Y>::CircularBufferXY(buffer_size), sprite(TFT_eSprite(tft)) {
  
  height = h ;
  width = w ;
  width_in_xunits = wxu ;
  height_in_yunits = hyu ;
  min_x = mx ;
  min_y = my ; 

  //sprite = TFT_eSprite(tft);
}

template<typename X, typename Y>
void CBXYSprite<X,Y>::setup() {
  sprite.createSprite(width, height);

  sprite.setColorDepth(8);

  // Define text datum and text colour for Sprites
  //sprite.setTextColor(TFT_WHITE);
  sprite.setTextDatum(MC_DATUM);
  //sample.add_hard(rand()%SCREEN_WIDTH, rand()%SCREEN_HEIGHT);
}

template<typename X, typename Y>
double CBXYSprite<X,Y>::x(double x_) const {
  return (x_ - min_x) * width * 1. / width_in_xunits ;
}

template<typename X, typename Y>
double CBXYSprite<X,Y>::y(double y_) const {
  //return height - (y_ - min_y) * height * 1. / height_in_yunits ;
  return height * 1. / (height_in_yunits) * (height_in_yunits + min_y - y_);
}



template<typename X, typename Y>
bool CBXYSprite<X,Y>::inside_sprite(double x_, double y_) const {
  return inside(x(x_), y(y_)) ;
}

// is (x_sprite, y_sprite) inside the sprite?
template<typename X, typename Y>
bool CBXYSprite<X,Y>::inside(double x_sprite, double y_sprite) const {
  if (x_sprite < - EPS || x_sprite > width + EPS) {
    return false;
  }
  return (y_sprite >= -EPS && y_sprite <= height + EPS) ;
}


template<typename X, typename Y>
void CBXYSprite<X,Y>::draw_lin_reg() {

  Serial.println("-----------------%%%%%%%%%%%%----------");
  
  double x_inter_sprite[2];
  double y_inter_sprite[2];
  double test ;
  uint16_t nb_intersect = 0 ;

  test = min_x ;
  x_inter_sprite[nb_intersect] = x(test);
  y_inter_sprite[nb_intersect] = y(this->lin_reg(test));
  Serial.println(x_inter_sprite[nb_intersect]);
  Serial.println(y_inter_sprite[nb_intersect]);
  
  double x_sprite(x_inter_sprite[nb_intersect]), y_sprite(y_inter_sprite[nb_intersect]);

  if (inside(x_inter_sprite[nb_intersect], y_inter_sprite[nb_intersect])) {
    nb_intersect++;
  }

  test = min_x + width_in_xunits ;
  x_inter_sprite[nb_intersect] = x(test);
  y_inter_sprite[nb_intersect] = y(this->lin_reg(test));
  if (inside(x_inter_sprite[nb_intersect], y_inter_sprite[nb_intersect])) {
    nb_intersect++;
  }

  if (nb_intersect < 2) {
    test = min_y ;
    x_inter_sprite[nb_intersect] = x(this->inv_lin_reg(test));
    y_inter_sprite[nb_intersect] = y(test);
    if (inside(x_inter_sprite[nb_intersect], y_inter_sprite[nb_intersect])) {
      nb_intersect++;
    }

    if (nb_intersect == 0) {
      return ;
    } else if (nb_intersect < 2) {
      test = min_y + height_in_yunits ;
      x_inter_sprite[nb_intersect] = x(this->inv_lin_reg(test));
      y_inter_sprite[nb_intersect] = y(test);
      if (inside(x_inter_sprite[nb_intersect], y_inter_sprite[nb_intersect])) {
        nb_intersect++;
      }
    }
  }
  Serial.println("Inter: " + nb_intersect);
  Serial.println("-----------");
  for(int i = 0 ; i < nb_intersect ; i++){
    Serial.println(x_inter_sprite[i]);
    Serial.println(y_inter_sprite[i]);
  }
  if (nb_intersect == 2) {
    sprite.drawLine(x_inter_sprite[0], y_inter_sprite[0], x_inter_sprite[1], y_inter_sprite[1], TFT_WHITE) ;
  }
}

template<typename X, typename Y>
void CBXYSprite<X,Y>::plot() {
  sprite.fillSprite(TFT_BLACK);
  sprite.setTextSize(5);

  sprite.drawLine(0, y(800), width, y(800), TFT_YELLOW) ;
  sprite.drawLine(0, y(1000), width, y(1000), TFT_ORANGE) ;
  sprite.drawLine(0, y(1200), width, y(1200), TFT_RED) ;
  
  this->make_lin_reg() ;
  //draw_lin_reg() ;

  int i ;
  for(i = this->full?this->inc(this->i_start):this->i_start ; i != this->i_insert ; i = this->inc(i)) {
    sprite.drawCircle(x(this->tab_x[i]), y(this->tab_y[i]), 1, TFT_BLUE) ;
  }

  sprite.pushSprite(0, 0);
}

template<typename X, typename Y>
void CBXYSprite<X,Y>::plot_left() {
  min_x = this->tab_x[this->i_start] ;
  
  plot() ;
}
