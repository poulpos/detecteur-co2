#ifndef data_structure_h
#define data_structure_h

#include <TFT_eSPI.h>

template<typename X, typename Y>
class CircularBufferXY {
  protected:
    int i_start ;
    int i_insert ;
    int size ;
    bool full ;
    X* tab_x ;
    Y* tab_y ;
    double y_0 ;
    double gradient ;
    int inc(int i) const;
  public:
    CircularBufferXY(int size);

    void add_hard(X x, Y y);
    void add_soft(X x, Y y);
    
    void clean();
    bool is_full() const;

    void make_lin_reg() ;
    double lin_reg(double x) const;
    double inv_lin_reg(double y) const;

    void toSerial() const ;
};

template<typename X, typename Y>
class CBXYSprite: public CircularBufferXY<X, Y> {
  private:
    uint16_t height ;
    uint16_t width ;
    TFT_eSprite sprite ;
    X width_in_xunits ;
    Y height_in_yunits ;
    X min_x ;
    Y min_y ;
    double x(double x_) const ;
    double y(double y_) const ;
    bool inside(double x_, double y_) const ;
    bool inside_sprite(double x_, double y_) const ;
    void draw_lin_reg() ;

  public:
    CBXYSprite(uint16_t w, uint16_t h, X wxu, Y hyu, X mx, Y my, TFT_eSPI* tft, int buffer_size) ;
    void plot() ;
    void plot_left() ;
    void setup() ;
};

// #include "data_structure.cpp"

#endif
