#include "Tone32.h"

#define BUZZER_PIN 16
#define BUZZER_CHANNEL 0

#define SMOOTH_FONTS

#include <TFT_eSPI.h>

#include <stdlib.h>
#include "data_structure.h"
#include "data_structure.cpp"


TFT_eSPI tft = TFT_eSPI();

TFT_eSprite sprite = TFT_eSprite(&tft);

#define SAMPLE_SIZE 24
#define SAMPLE_HEIGHT 100
#define SAMPLE_WIDTH 240

#define SCREEN_HEIGHT 135
#define SCREEN_WIDTH 240

#define EPS 1e-2


#define MIN_CO2 400

#define TXD2 21         // sÃ©rie capteur TX
#define RXD2 22         // sÃ©rie capteur RX





CBXYSprite<float, unsigned long> sample(240, 135, 240, 1200, 0, 400, &tft, SAMPLE_SIZE);

int i = 0 ;

//uint16_t sample[SAMPLE_SIZE] ;
//uint16_t sample_start = 0 ;
//uint16_t sample_end = 0 ;

#define INC(i) ((i<SAMPLE_SIZE-1)?i+1:0)










volatile uint32_t DebounceTimer = 0;

byte CO2req[] = {0xFE, 0X04, 0X00, 0X03, 0X00, 0X01, 0XD5, 0XC5};
byte ABCreq[] = {0xFE, 0X03, 0X00, 0X1F, 0X00, 0X01, 0XA1, 0XC3}; 
byte disableABC[] = {0xFE, 0X06, 0X00, 0X1F, 0X00, 0X00, 0XAC, 0X03};  // Ã©crit la pÃ©riode 0 dans le registre HR32 Ã  adresse 0x001f
byte enableABC[] = {0xFE, 0X06, 0X00, 0X1F, 0X00, 0XB4, 0XAC, 0X74}; // Ã©crit la pÃ©riode 180
byte clearHR1[] = {0xFE, 0X06, 0X00, 0X00, 0X00, 0X00, 0X9D, 0XC5}; // ecrit 0 dans le registe HR1 adresse 0x00
byte HR1req[] = {0xFE, 0X03, 0X00, 0X00, 0X00, 0X01, 0X90, 0X05}; // lit le registre HR1 (vÃ©rifier bit 5 = 1 )
byte calReq[] = {0xFE, 0X06, 0X00, 0X01, 0X7C, 0X06, 0X6C, 0XC7}; // commence la calibration background
byte Response[20];
uint16_t crc_02;
int ASCII_WERT;
int int01, int02, int03;
unsigned long ReadCRC;      // CRC Control Return Code 





void send_Request (byte * Request, int Re_len)
{
  while (!Serial1.available())
  {
    Serial1.write(Request, Re_len);   // Send request to S8-Sensor
    delay(50);
  }

  Serial.print("Requete : ");
  for (int02 = 0; int02 < Re_len; int02++)    // Empfangsbytes
  {
    Serial.print(Request[int02],HEX);
    Serial.print(" ");
  }
  Serial.println();
  
}

void read_Response (int RS_len)
{
  int01 = 0;
  while (Serial1.available() < 7 ) 
  {
    int01++;
    if (int01 > 10)
    {
      while (Serial1.available())
        Serial1.read();
      break;
    }
    delay(50);
  }

  Serial.print("Reponse : ");
  for (int02 = 0; int02 < RS_len; int02++)    // Empfangsbytes
  {
    Response[int02] = Serial1.read();
    
    Serial.print(Response[int02],HEX);
    Serial.print(" ");
  }
  Serial.println();
}

unsigned short int ModBus_CRC(unsigned char * buf, int len)
{
  unsigned short int crc = 0xFFFF;
  for (int pos = 0; pos < len; pos++) {
    crc ^= (unsigned short int)buf[pos];   // XOR byte into least sig. byte of crc
    for (int i = 8; i != 0; i--) {         // Loop over each bit
      if ((crc & 0x0001) != 0) {           // If the LSB is set
        crc >>= 1;                         // Shift right and XOR 0xA001
        crc ^= 0xA001;
      }
      else                            // else LSB is not set
        crc >>= 1;                    // Just shift right
    }
  }  // Note, this number has low and high bytes swapped, so use it accordingly (or swap bytes)
  return crc;  
}

unsigned long get_Value(int RS_len)
{

// Check the CRC //
  ReadCRC = (uint16_t)Response[RS_len-1] * 256 + (uint16_t)Response[RS_len-2];
  if (ModBus_CRC(Response, RS_len-2) == ReadCRC) {
    // Read the Value //
    unsigned long val = (uint16_t)Response[3] * 256 + (uint16_t)Response[4];
    return val * 1;       // S8 = 1. K-30 3% = 3, K-33 ICB = 10
  }
  else {
    Serial.print("CRC Error");
    return 99;
  }
}





TFT_eSprite sprite_value = TFT_eSprite(&tft);









void setup() {
  Serial.begin(115200);
  Serial.println((10));
  sample.toSerial();

  Serial1.begin(9600, SERIAL_8N1, RXD2, TXD2);


  send_Request(ABCreq, 8);
  read_Response(7);
  Serial.print("PÃ©riode ABC : ");
  Serial.printf("%02ld", get_Value(7));
  Serial.println("||||||||||||||||||||||");

  


  tft.init();
  tft.fillScreen(TFT_BLACK);
  tft.setRotation(1);

  //tft.loadFont("Roboto_Thin_24");
  //tft.showFont(1000);

  sample.setup() ;

  sprite_value.createSprite(50, 10);
  
  Serial.printf("Screen: %dx%d\n", tft.height(), tft.width());
}

void loop() {
  
  //tft.startWrite();

  //sample.add_hard(i, rand()%SCREEN_HEIGHT);
  i += 10 ;

  
  sample.toSerial();
  //sprite
  Serial.println("---------------------------xxxiiiiiii");


  // lecture du capteur
  send_Request(CO2req, 8);
  read_Response(7);
  unsigned long CO2 = get_Value(7);
  sample.add_hard(i, CO2);
  CO2 -= 40 ;
  
  String CO2s = String(CO2) + "ppm" ;
  Serial.println(CO2s);


  
  Serial.println(CO2s);

  sample.plot_left();

  sprite_value.fillSprite(TFT_BLACK);  
  sprite_value.setTextSize(1);
  sprite_value.drawString(CO2s, 0, 0);
  sprite_value.pushSprite(0, 120);
  
  Serial.println("---------------------------xxx");
  //tft.endWrite();
  delay(1000);
  //tft.showFont(1000);
}
