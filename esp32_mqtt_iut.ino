#include <WiFi.h>
#include <WiFiMulti.h> 
#include <PubSubClient.h>

//WIFI
const char* ssid = "xxx";
const char* password = "xxx"; 

//MQTT
const char* mqtt_server = "iot.iut-blagnac.fr"; // Adresse IP ou nom du broker MQTT
const int mqttPort = 1883; // port utilisé par le broker 
long tps=0;

WiFiMulti WiFiMulti;
WiFiClient espClient;
PubSubClient client(espClient);

void setup()
{
  Serial.begin(115200);
  setup_wifi();
  setup_mqtt();
  client.publish("sandbox/esp/test", "Hello from ESP32");
  pinMode(2, OUTPUT);
}

void loop()
{
  reconnect();
  client.loop(); 
  //On utilise pas un delay pour ne pas bloquer la réception de messages 
  if (millis()-tps>1000)
  {
    tps=millis();
    float temp = random(30);
    mqtt_publish("sandbox/esp/test/qqchose",temp);
    Serial.print("qqchose : ");
    Serial.println(temp); 
  }
}

void setup_wifi(){
  //connexion au wifi
  WiFiMulti.addAP(ssid, password);
  while ( WiFiMulti.run() != WL_CONNECTED ) {
    delay ( 500 );
    Serial.print ( "." );
  }
  Serial.println("");
  Serial.println("WiFi connecté");
  Serial.print("MAC : ");
  Serial.println(WiFi.macAddress());
  Serial.print("Adresse IP : ");
  Serial.println(WiFi.localIP());
}

void setup_mqtt()
{
  client.setServer(mqtt_server, mqttPort);
  client.setCallback(callback);//Déclaration de la fonction de souscription
  reconnect();
}

//Callback doit être présent pour souscrire a un topic et de prévoir une action 
void callback(char* topic, byte *payload, unsigned int length)
{
   Serial.println("-------Nouveau message du broker mqtt-----");
   Serial.print("topic:");
   Serial.println(topic);
   Serial.print("donnee:");
   Serial.write(payload, length);
   Serial.println();
   if ((char)payload[0] == '1') {
     Serial.println("LED ON");
      digitalWrite(2,HIGH); 
   } else {
     Serial.println("LED OFF");
     digitalWrite(2,LOW); 
   }
 }


void reconnect()
{
  while (!client.connected())
  {
    Serial.println("Connection au serveur MQTT ...");
    if (client.connect("ESP32Client", "student", "student"))
    {
      Serial.println("MQTT connecté");
    }
    else 
    {
      Serial.print("echec, code erreur= ");
      Serial.println(client.state());
      Serial.println("nouvel essai dans 2s");
    delay(2000);
    }
  }
  client.subscribe("sandbox/esp/test/led");//souscription au topic led pour commander une led
}

//Fonction pour publier un float sur un topic 
void mqtt_publish(String topic, float t)
{
  char top[topic.length()+1];
  topic.toCharArray(top,topic.length()+1);
  char t_char[50];
  String t_str = String(t);
  t_str.toCharArray(t_char, t_str.length() + 1);
  client.publish(top,t_char);
}
