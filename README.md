# Détecteur CO<sub>2</sub>[^1]

Code source d'un matériel de mesure du CO<sub>2</sub> ambiant.

Hardware :
- Microcontrôleur : ESP32 TTGO T-Display
- Capteur CO<sub>2</sub> : type NDIR, Senseair S8 LP

_________
[^1]: Librement inspiré du [travail de Grégoire Rinolfi](https://co2.rinolfi.ch/), recensé dans le [projet CO<sub>2</sub>](http://projetco2.fr/).
